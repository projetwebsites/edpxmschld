/*
    This class will contain all actions related to Enrollment
*/
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using edpxmschdl.Models.InterfacesModels;
using edpxmschdl.Models.ViewModels;
using edpxmschdl.Models;

namespace edpxmschdl.Controllers
{
    public class EnrollmentController : Controller
    {
        private IEnrollmentRepository m_repository; // connection to the db through the interface
        //private IEnumerable<Enrollment> Enrollments {get; set;}
        public int pageSize = 10;
        public EnrollmentController(IEnrollmentRepository repository) => m_repository = repository;
        public IActionResult Index(int session=-1, string schoolYear="", string courseName="All" )
        {
            var enrollmentIndex = new EnrollementIndexViewModel();
            //var totalInscrits;
            Filter filter = new Filter();
            filter.Session = Session.Winter;// can get current
            filter.SchoolYear = "2020"; // can get current
            filter.Course = courseName;
            // Checking if we get all the courses or a specific one to get tha data from the db
            if(filter.Course == "All")
            {
                var enrollments = m_repository.Enrollments.Include(c => c.Course);
                                                //.Distinct();
                                                //.GroupBy(c => c.CourseID);
                                                //.OrderBy(c => c.);//.GroupBy(c => c.CourseID);
                //getting the numbers of student per courses with linq
                IQueryable<EnrollmentCourseGroup> enrolledByCourse = 
                    from enrollement in m_repository.Enrollments
                    group enrollement by enrollement.CourseID into courseGroupe
                    select new EnrollmentCourseGroup()
                    {
                        CourseID = courseGroupe.Key,
                        StudentCount = courseGroupe.Count()
                    };
                filter.TotalEnrolled = m_repository.Enrollments.Count();
                enrollmentIndex.EnrollmentGroup = enrolledByCourse;
                enrollmentIndex.Filter = filter;
                enrollmentIndex.Enrollments = enrollments as IEnumerable<Enrollment>;
            }
            else
            {
                var enrollments = m_repository.Enrollments.Include(s => s.Student)
                                                            .Include(c => c.Course)
                                                            .Where(c => c.Course.Sigle == filter.Course);
                filter.TotalEnrolled = m_repository.Enrollments.Include(c => c.Course)
                                                                .Where(c => c.Course.Sigle == filter.Course).Count();
                
                enrollmentIndex.Filter = filter;
                enrollmentIndex.Enrollments = enrollments as IEnumerable<Enrollment>;
            }
            //EnrollmentIndex 
            return View(enrollmentIndex);
        }
    }
}