/*
    Cette classe va contenir toutes méthodes liées à la réservation des examens par des étudiants
*/
using Microsoft.AspNetCore.Mvc;
using edpxmschdl.Models;

//using edpxmschdl.Models;
namespace edpxmschdl.Controllers
{
    public class ExamReservationController : Controller
    {
        public ViewResult Index() => View("ExamReservation");
        [HttpPost]
        public ViewResult ExamReservation(ExamReservation model) => View("", model);
        [HttpGet]
        public ViewResult ExamReservation() => View(new ExamReservation());
    }
}
