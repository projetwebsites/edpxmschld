/*
    This class will contain all actions related to courses
*/
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using edpxmschdl.Models.InterfacesModels;
using edpxmschdl.Models.ViewModels;

namespace edpxmschdl.Controllers
{
    public class CourseController : Controller
    {
        private ICourseRepository m_repository; // our connection to the database
        public int PageSize = 4;                // number of courses to display per pages
        public CourseController(ICourseRepository repository) => m_repository = repository;
        //[HttpGet]
        public ViewResult Index(string departement = "null", int coursePage = 1)
        {
            var courseIndex = new CourseIndexViewModel {
                Courses = m_repository.Courses //querying courses from db
                            //.Where(c => departement == null || c.Departement == departement)
                            .OrderBy(c => c.CourseID)
                            .Skip((coursePage - 1) * PageSize)
                            .Take(PageSize),
                PagingInfo = new PagingInfo{
                    CurrentPage = coursePage,
                    ItemsPerPage = PageSize,
                    TotalItems = m_repository.Courses.Count()//Bien compter en fonction du departement pg 255
                },
                //CurrentDepartement = departement
            };
            return View(courseIndex);
        }
    }
}