/*
    This class will contain all actions related to supervisors
*/
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using edpxmschdl.Models.InterfacesModels;
using edpxmschdl.Models.ViewModels;

namespace edpxmschdl.Controllers
{
    public class SupervisorController : Controller
    {
        private ISupervisorRepository m_repository; //connection to the database
        public int PageSize = 5;                    // number of item to display per page
        public SupervisorController(ISupervisorRepository repository) => m_repository = repository;
        public ViewResult Index(int supPage = 1)
        {
            var supervisorIndex = new SupervisorIndexViewModel{
                Supervisors = m_repository.Supervisors
                                .OrderBy(s => s.SupervisorId)
                                .Skip((supPage -1)* PageSize)
                                .Take(PageSize),
                PagingInfo = new PagingInfo {
                    CurrentPage = supPage,
                    ItemsPerPage = PageSize,
                    TotalItems = m_repository.Supervisors.Count()
                }
            };
            return View(supervisorIndex);
        }
    }
}