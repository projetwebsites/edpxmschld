/*
    This class will contain all actions related to students
*/
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using edpxmschdl.Models.InterfacesModels;
using edpxmschdl.Models.ViewModels;

namespace edpxmschdl.Controllers
{
    public class StudentController : Controller
    {
       private IStudentRepository m_repository; // connection to the database
       public int PageSize = 5;                 // number of student to display per page
       public StudentController(IStudentRepository repository) => m_repository = repository;
       public ViewResult Index(int studentPage = 1)
       {
           var studentIndex = new StudentIndexViewModel{
                Students = m_repository.Students
                            .OrderBy(s => s.StudentId)
                            .Skip((studentPage -1) * PageSize)
                            .Take(PageSize),
                PagingInfo = new PagingInfo {
                        CurrentPage = studentPage,
                        ItemsPerPage = PageSize,
                        TotalItems = m_repository.Students.Count()
                }
           };
           return View(studentIndex);
       }
    }
}