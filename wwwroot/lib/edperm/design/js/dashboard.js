﻿

$(function () {
    //something here
    // the following lines display tooltips when admin hovers upon some buttons
    function showTooltip(a) {
        var bullet = a;  
        var toolTipSet = $(".read_insight"); 
        var tooltipDisplayer = $(".insights");
      

        bullet.mouseover(function () {
            var bulletBorderColor = bullet.css("border-color");
            tooltipDisplayer.css({ "background-color": bulletBorderColor });
            var getTooltipText = bullet.attr("title");
            tooltipDisplayer.css({ 'visibility': 'visible' });
            toolTipSet.html(getTooltipText);
        });
        bullet.mouseout(function () {
            tooltipDisplayer.css({ 'visibility': 'hidden' });
        });
    }

    var cours_one = $(".cours_one");
    showTooltip(cours_one);

    var cours_two = $(".cours_two");
    showTooltip(cours_two);

    var cours_three = $(".cours_three");
    showTooltip(cours_three);

    var cours_four = $(".cours_four");
    showTooltip(cours_four);
     
});
