/* 
    Course Interface repository to provide access to Course object from the database.
    Course class that is going to depend on this interface won't know details on how Courses are saved or got
*/
using System.Linq;

namespace edpxmschdl.Models.InterfacesModels
{
    public interface ICourseRepository
    {
        IQueryable<Course> Courses { get; } //queried courses from the database
        void SaveCourse(Course course);     // make interface able to changes made to courses
        //delete etc...
    }
}