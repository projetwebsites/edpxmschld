/* 
    Student Interface repository is to provide access to Student object from the database.
    Student class that is going to depend on this interface won't know details on how Students are saved or got
*/
using System.Linq;

namespace edpxmschdl.Models.InterfacesModels
{
    public interface IStudentRepository
    {
        IQueryable<Student> Students { get; }       //to query Students from the database
        void SaveStudent(Student student);          //make interface to able to make changes made to Student
    }
}