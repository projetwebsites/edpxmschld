/* 
    Supervisor Interface repository is to provide access to Supervisor object from the database.
    Supervisor class that is going to depend on this interface won't know details on how supervisor are saved or got
*/
using System.Linq;

namespace edpxmschdl.Models.InterfacesModels
{
    public interface ISupervisorRepository
    {
        IQueryable<Supervisor> Supervisors{get;} // to query supervisor info from the database
        //void SaveSupervisor(Supervisor supervisor); // make interface able to make changes to supervisor table
    }
}