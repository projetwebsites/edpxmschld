/* 
    Enrollment Interface repository to provide access to Enrollment object from the database.
    Enrollment class that is going to depend on this interface won't know details on how Enrollment are saved or got
*/
using System.Linq;
using System.Collections.Generic;

namespace edpxmschdl.Models.InterfacesModels
{
    public interface IEnrollmentRepository
    {
        //IEnumerable<Enrollment> Enrollments(); // get all the enrollment from student
        IQueryable<Enrollment> Enrollments {get;}
    }
}