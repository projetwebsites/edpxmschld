/*
    Cette classe va servir à récupérer les informations entrées par les étudiants
    dans le formulaire de réservation
    -
    This model will be use to map information that student will enter from the reservation form
    :
*/
using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace edpxmschdl.Models
{
    public class ExamReservation
    {
        [BindNever]
        public int ReservationId {get; set;}

        [Required(ErrorMessage = "SVP Veuillez entrer votre numéro d'étudiant")]
        public int StudentId {get; set;}

        [Required(ErrorMessage = "SVP Veuillez entrer votre adresse électronique")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "SVP Veuillez entrer Une adresse électronique valide")]
        public string Email {get; set;} // email seulement pour la réservation si différent de celui de l'école

        [Required(ErrorMessage = "SVP Veuillez selectionner le cours")]
        public int CoursID {get; set;}

        [Required(ErrorMessage = "SVP Veuillez entrer la date de l'examen")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateEtHeureExamen {get; set;}

        [Required(ErrorMessage = "SVP Veuillez selectionner le type de l'examen")]
        public string TypeDExamen {get; set;}

        [Required(ErrorMessage = "SVP Veuillez entrer le type d'endroit où vous passerez l'examen")]
        public string TypeDEndroit {get; set;}

        [Required(ErrorMessage = "SVP Veuillez entrer l'adresse où vous comptez passer l'examen")]
        public string AdresseComplete {get; set;}

        [Required(ErrorMessage = "Avez-vous déjà trouvé un superviseur?")]
        public bool? EstSupervisE {get; set;}

        [Required(ErrorMessage = "SVP veuillez entrer le nom du superviseur")]
        public string SuperviseurNom {get; set;}

        [Required(ErrorMessage = "SVP Veuillez entrer l'email du superviseur")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "SVP Veuillez entrer Une adresse électronique valide")]
        public string SuperviseurEmail {get; set;}

        [Required(ErrorMessage = "SVP veuillez entrer le numéro de téléphone du superviseur")]
        public string SuperviseurTelephone {get; set;}
        [BindNever]
        public bool? EstFacturable {get; set;}

        // vpnt servir de référence à la clé primaire utilisé dans le formulaire ci haut
        public Student Student;
        public Course Course;
        public Adress Adresse;
    }

    public class Adress
    {
        public string Country {get; set;}
        public string StateOrProvince {get; set;}
        public string Town {get; set;}
        public string Street {get; set;}
        public int AdressId {get; set;}
    }
}