/*
    Classe qui va contenir les options de connexion à la base de données
*/
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.DependencyInjection;

namespace edpxmschdl.Models.DbContextModels
{
    public class ApplicationDbContext : DbContext {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            :base(options){}
        // Traitement des éléments venant de la base de données
        // DbSet<TableName>
        public DbSet<Course> Courses {get; set;}
        public DbSet<Student> Students {get;set;}
	    public DbSet<Supervisor> Supervisors {get;set;}
        public DbSet<Enrollment> Enrollments {get; set;}// table des inscriptions
    }
}
