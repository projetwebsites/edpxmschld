-- INSERT INTO Courses(Sigle, Nom, Credits)
-- VALUES
-- ('INFO4006', 'Programmation UNIX', 3),
-- ('MATH1063', 'Trigonométrie', 3),
-- ('MATH1163', 'Intégrale I', 3)

-- SELECT Courses.[Sigle] AS SIGLE
-- FROM Enrollments
-- LEFT JOIN Courses ON Enrollments.CourseID = Courses.CourseID
-- GROUP BY SIGLE
-- EXEC sp_columns Enrollments
--EXEC sp_fkeys @pktable_name = 'Enrollments'
SELECT DISTINCT Courses.[Sigle] AS SIGLE, Enrollments.CourseID 
FROM Enrollments
LEFT JOIN Courses ON Enrollments.CourseID = Courses.CourseID
GO