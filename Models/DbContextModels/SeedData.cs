/*
    Class that will prepopulate some data in the database
*/
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace edpxmschdl.Models.DbContextModels
{
    public static class SeedData
    {
        /*
            The static InitializeDb method receives an IApplicationBuilder argument, which is the interface
            used in the Configure method of the Startup class to register middleware components to handle HTTP
            requests, and this is where I will ensure that the database has content.

            The InitializeDb method obtains an ApplicationDbContext object through the
            IApplicationBuilder interface and calls the Database.Migrate method to ensure that the migration has
            been applied, which means that the database will be created and prepared so that it can store
            specific objects. Next, the number of different objects in the database are checked. If there are no objects in the
            database, then the database is populated using a collection of per specification objects using the AddRange method
            and then written to the database using the SaveChanges method.
        */
        public static void InitializeDb(IApplicationBuilder app)
        {
            ApplicationDbContext context = app.ApplicationServices.GetRequiredService<ApplicationDbContext>();
            context.Database.Migrate();
            //we check first the courses in the database
            if(!context.Courses.Any())
            {
                context.Courses.AddRange( // faut ajouter la faculté
                    new Course {
                        Sigle = "INFO1001", Nom = "Principe de programmation I", Credits = 3
                    },
                    new Course {
                        Sigle = "INFO2012", Nom = "Introduction aux Algorithmes", Credits = 3
                    },
                    new Course {
                        Sigle = "INFO3020", Nom = "Introduction aux Réseaux", Credits = 3
                    },
                    new Course {
                        Sigle = "INFO4008", Nom = "Multimédia", Credits = 3
                    },
                    new Course {
                        Sigle = "MATH2652", Nom = "Algèbre Matricielle", Credits = 3
                    },
                    new Course {
                        Sigle = "MATH1163", Nom = "Introduction aux intégrales", Credits = 3
                    },
                    new Course {
                        Sigle = "MATH1063", Nom = "Trigonométrie", Credits = 3
                    },
                    new Course {
                        Sigle = "MATH2263", Nom = "Intégrales II", Credits = 3
                    }
                );
                context.SaveChanges();
            }
            // we check differents students
            if(!context.Students.Any())
            {
                context.Students.AddRange(
                    new Student{
                        Ni        = "A00158681",
                        FirstName = "Mukeya",
                        LastName  = "Kassindye",
                        Email     = "djasy3@gmail.com",
                        DayPhoneNumber = "5065888922"
                    },
                    new Student{
                        Ni        = "A00158682",
                        FirstName = "Kasongo",
                        LastName  = "Mwila",
                        Email     = "k.mwila@unilu.cd",
                        DayPhoneNumber = "5060001234"
                    },
                    new Student{
                        Ni        = "A00158683",
                        FirstName = "Sheshi",
                        LastName  = "Maata",
                        Email     = "s.maata@unilu.cd",
                        DayPhoneNumber = "5061234567"
                    },
                    new Student{
                        Ni        = "A00158680",
                        FirstName = "Simba",
                        LastName  = "Matata",
                        Email     = "s.maata@unilu.cd",
                        DayPhoneNumber = "5061234567"
                    },
                    new Student{
                        Ni        = "A00158670",
                        FirstName = "Kalulu",
                        LastName  = "Mukalamushi",
                        Email     = "s.maata@unilu.cd",
                        DayPhoneNumber = "5061234567"
                    },
                    new Student{
                        Ni        = "A00158660",
                        FirstName = "Nkalamo",
                        LastName  = "Mumba",
                        Email     = "s.maata@unilu.cd",
                        DayPhoneNumber = "5061234567"
                    }
                );
                context.SaveChanges();
            }
            if(!context.Supervisors.Any())
            {
                context.Supervisors.AddRange(
                    new Supervisor{
                        FirstName = "Akin",
                        LastName  = "Fatoki",
                        Email = "akin.sup@unilu.cd",
                        DayPhoneNumber = "5061234567",
                        NightPhoneNumber = "5061234567",
                        IsApprouved = false
                    },
                    new Supervisor{
                        FirstName = "Lucie",
                        LastName  = "Bourque",
                        Email = "lucie.sup@unilu.cd",
                        DayPhoneNumber = "5061234567",
                        NightPhoneNumber = "5061234567",
                        IsApprouved = true
                    }
                );
                context.SaveChanges();
            }
            if(!context.Enrollments.Any())
            {
                context.Enrollments.AddRange(
                    new Enrollment{
                        StudentID = context.Students.First( s => s.StudentId == 1).StudentId,
                        CourseID = context.Courses.First(c => c.CourseID == 1).CourseID,
                        AnneeAcademique = "2020",
                        Session = Session.Winter
                    },
                    new Enrollment{
                        StudentID = 1,
                        CourseID = 2,
                        AnneeAcademique = "2020",
                        Session = Session.Winter
                    },
                    new Enrollment{
                        StudentID = 1,
                        CourseID = 3,
                        AnneeAcademique = "2020",
                        Session = Session.Winter
                    },
                    new Enrollment{
                        StudentID = 2,
                        CourseID = 1,
                        AnneeAcademique = "2020",
                        Session = Session.Winter
                    },
                    new Enrollment{
                        StudentID = 2,
                        CourseID = 2,
                        AnneeAcademique = "2020",
                        Session = Session.Winter
                    },
                    new Enrollment{
                        StudentID = 2,
                        CourseID = 3,
                        AnneeAcademique = "2020",
                        Session = Session.Winter
                    },
                    new Enrollment{
                        StudentID = 2,
                        CourseID = 4,
                        AnneeAcademique = "2020",
                        Session = Session.Winter
                    },
                    new Enrollment{
                        StudentID = 3,
                        CourseID = 1,
                        AnneeAcademique = "2020",
                        Session = Session.Winter
                    },
                    new Enrollment{
                        StudentID = 3,
                        CourseID = 2,
                        AnneeAcademique = "2020",
                        Session = Session.Winter
                    },
                    new Enrollment{
                        StudentID = 3,
                        CourseID = 3,
                        AnneeAcademique = "2020",
                        Session = Session.Winter
                    },
                    new Enrollment{
                        StudentID = 5,
                        CourseID = 5,
                        AnneeAcademique = "2020",
                        Session = Session.Winter
                    },
                    new Enrollment{
                        StudentID = 5,
                        CourseID = 1,
                        AnneeAcademique = "2020",
                        Session = Session.Winter
                    }
                );
                context.SaveChanges();
            }
        }
    }
}