/*
    The EFEnrollment class uses the interface to manipulate Enrollment info from and to the database
*/
using System.Collections.Generic;
using System.Linq;
using edpxmschdl.Models.DbContextModels;
using edpxmschdl.Models.InterfacesModels;
using Microsoft.EntityFrameworkCore;

namespace edpxmschdl.Models.EFModels
{
    public class EFEnrollmentRepository : IEnrollmentRepository
    {
        private ApplicationDbContext m_context; // getting connection to the db
        public EFEnrollmentRepository(ApplicationDbContext context) => m_context = context;
        // public IEnumerable<Enrollment> Enrollments()
        // {
        //     //IQuery inside of IEnumerable
        //     IQueryable<Enrollment> enrollmentsData = m_context.Enrollments;

        //     enrollmentsData = enrollmentsData.Include(e => e.Course).Include(e => e.Student);

        //     return enrollmentsData;
        // } //=> m_context.Enrollments;
        public IQueryable<Enrollment> Enrollments => m_context.Enrollments;
    }
}