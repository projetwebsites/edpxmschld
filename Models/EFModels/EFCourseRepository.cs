/*
    The EFCourseRepository class uses the interface to manipulate objects from and to the database
*/
using System.Collections.Generic;
using System.Linq;
using edpxmschdl.Models.DbContextModels;
using edpxmschdl.Models.InterfacesModels;

namespace edpxmschdl.Models.EFModels
{
    public class EFCourseRepository : ICourseRepository
    {
        private ApplicationDbContext m_context;
        public EFCourseRepository(ApplicationDbContext context) => m_context = context;
        public IQueryable<Course> Courses => m_context.Courses;
        /*
            Save method adds a course to the repository if the CourseIDis 0;
            Otherwise, it applies any changes to the existing entry of that course in the database.
            An update is performed when the CourseIDis not equal to 0
        */
        public void SaveCourse(Course course)
        {
            if(course.CourseID== 0)
            {
                m_context.Courses.Add(course);
            }
            else
            {
                // Getting a course from the repository to verify it with the CourseIDprovided from the from or user input
                Course dbEntry = m_context.Courses.FirstOrDefault(c => c.CourseID== course.CourseID);
                if(dbEntry != null)
                {
                    dbEntry.Sigle   = course.Sigle;
                    dbEntry.Nom     = course.Nom;
                    dbEntry.Credits = course.Credits;
                    //dbEntry.Faculty = course.Faculty;
                }
            }
            // save changes to the database
            m_context.SaveChanges();
        }
    }
}
