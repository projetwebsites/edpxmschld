/*
    The EFSupervisor class uses the interface to manipulate Supervisor info from and to the database
*/
using System.Collections.Generic;
using System.Linq;
using edpxmschdl.Models.DbContextModels;
using edpxmschdl.Models.InterfacesModels;

namespace edpxmschdl.Models.EFModels
{
    public class EFSupervisorRepository : ISupervisorRepository
    {
        private ApplicationDbContext m_context;
        public EFSupervisorRepository(ApplicationDbContext context) => m_context = context;
        public IQueryable<Supervisor> Supervisors => m_context.Supervisors;
    }
}