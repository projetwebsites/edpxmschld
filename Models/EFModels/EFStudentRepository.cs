/*
    The EFStudentRepository class uses the interface to manipulate Students objects from and to the database
*/
using System.Collections.Generic;
using System.Linq;
using edpxmschdl.Models.DbContextModels;
using edpxmschdl.Models.InterfacesModels;

namespace edpxmschdl.Models.EFModels
{
    public class EFStudentRepository : IStudentRepository
    {
        private ApplicationDbContext m_context;

        public EFStudentRepository(ApplicationDbContext context) => m_context = context;
        public IQueryable<Student> Students => m_context.Students;
        /*
            Save method adds a student to the repository if the StudentID is 0;
            Otherwise, it applies any changes to the existing entry of that student in the database.
            An update is performed when the StudenID is not equal to 0
        */
        public void SaveStudent(Student student)
        {
            if(student.StudentId == 0)
            {
                m_context.Students.Add(student);
            }
            else
            {   // Getting a student from the repository to verify it with the StudentId provided from the from or user input
                Student dbEntry = m_context.Students.FirstOrDefault(s => s.StudentId == student.StudentId);
                if(dbEntry != null)
                {
		            dbEntry.Ni = student.Ni;
                    dbEntry.FirstName = student.FirstName;
                    dbEntry.LastName  = student.LastName;
                    dbEntry.Email     = student.Email;
                    dbEntry.DayPhoneNumber = student.DayPhoneNumber;
                    dbEntry.NightPhoneNumber = student.NightPhoneNumber;
                }
            }
            m_context.SaveChanges();
        }
    }
}
