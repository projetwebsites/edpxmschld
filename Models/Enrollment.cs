using System;

namespace edpxmschdl.Models
{
    /*
        Ici la classe enrollment : inscription.
        Ces données d'inscriptions nous viennent directement de l'université qui les envoit à l'éducation
        permanente / en tant que 2 entités séparés
    */
    public class Enrollment
    {
        public int EnrollmentId {get; set;}
        public int StudentID {get; set;} // to create a required relationship as a foreign key
        public string AnneeAcademique {get; set;} //YYYY
        public int CourseID {get; set;} // to create a required relationship as a foreign key
        public Session Session { get; set; }

        // enrollement date

        // stillenrolled

        //Navigation properties
        public Student Student { get; set; }
        public Course Course { get; set; }
    }

    public class Course
    {
        public int CourseID{get; set;}
        public string Sigle {get; set;}
        public string Nom {get; set;}
        public int Credits {get; set;}
        //public string Faculty {get; set;}
        //public string Departement {get; set;}
    }

    public enum Session
    {
        Winter, Spring, Fall
    }
}