namespace edpxmschdl.Models
{
    public class Student
    {
        public int StudentId {get; set;}
        public string Ni {get; set; }
        public string FirstName {get; set;}
        public string LastName {get; set;}
        public string Email {get; set;}
        public string DayPhoneNumber {get; set;}
        public string NightPhoneNumber {get; set;}
    }
}
//TM00002571034