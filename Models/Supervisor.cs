using System;

namespace edpxmschdl.Models
{
    public class Supervisor
    {
        public int SupervisorId {get; set;}
        public string FirstName {get; set;}
        public string LastName {get; set;}
        public string Email {get; set;}
        public string DayPhoneNumber {get; set;}
        public string NightPhoneNumber {get; set;}
        public bool IsApprouved {get; set;}
    }

    public class Contract
    {
        public int ContractId {get; set;}
        public int SupervisorId {get; set;}
        public bool IsSigned {get; set;}
        public DateTime ConfirmedDate {get; set;}
        public DateTime SignedDate{get; set;}

        //navigation
        public Supervisor Supervisor;
    }
}