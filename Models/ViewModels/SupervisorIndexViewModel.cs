/*
    This class will represent the page to display Supervisor together with the pagination system
*/
using System.Collections.Generic;
using edpxmschdl.Models;

namespace edpxmschdl.Models.ViewModels
{
    public class SupervisorIndexViewModel
    {
        public IEnumerable<Supervisor> Supervisors { get; set; } // get list of all supervisors
        public PagingInfo PagingInfo { get; set; }               // get number of supervisors to display per page
    }
}