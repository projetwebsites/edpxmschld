/*
    This class will represent the page to display courses together with the pagination system
*/
using System.Collections.Generic;
using edpxmschdl.Models;

namespace edpxmschdl.Models.ViewModels
{
    public class CourseIndexViewModel
    {
        public IEnumerable<Course> Courses {get; set;} //get list of courses
        public string CurrentDepartement {get; set;}
        public PagingInfo PagingInfo {get; set;}        // get number of courses to be display per page
    }
}