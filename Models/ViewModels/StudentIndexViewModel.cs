/*
    This class will represent the page to display Students together with the pagination system
*/
using System.Collections.Generic;
using edpxmschdl.Models;

namespace edpxmschdl.Models.ViewModels
{
    public class StudentIndexViewModel
    {
        public IEnumerable<Student> Students {get; set;} // get list of all student
        public PagingInfo PagingInfo {get; set;}            // get number of students to dispaly per page
    }
}