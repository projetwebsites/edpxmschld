/*
    This class will represent the page to display Enrollement together with the pagination system
*/
using System.Collections.Generic;

namespace edpxmschdl.Models.ViewModels
{
    public class EnrollementIndexViewModel
    {
        public IEnumerable<Enrollment> Enrollments {get; set;}
        public IEnumerable<EnrollmentCourseGroup> EnrollmentGroup {get; set; }
        public Filter Filter {get; set;}
        public PagingInfo PagingInfo {get;set;} // get number of enrollment to be displayed per page
    }
    // classe qui va constituer un filtre
    public class Filter
    {
        public Session Session {get; set;}
        public string SchoolYear {get; set;}
        public string Course {get; set;}
        public int TotalEnrolled {get; set;}
        public int TotalDropped {get; set;}
    }
    // classe to get number of student per corurses
    public class EnrollmentCourseGroup
    {
        public int CourseID {get; set;}
        public int StudentCount {get; set;}
    }
}