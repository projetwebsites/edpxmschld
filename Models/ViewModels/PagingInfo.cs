/*
    This class is going to provide the pagination system on Index pages
*/
using System;

namespace edpxmschdl.Models.ViewModels
{
    public class PagingInfo
    {
        public int TotalItems {get; set;}
        public int ItemsPerPage {get; set;}
        public int CurrentPage {get;set;}
        public int TotalPages => (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage);
    }
}