using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using edpxmschdl.Models.DbContextModels;
using edpxmschdl.Models.EFModels;
using edpxmschdl.Models.InterfacesModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace edpxmschdl
{
    public class Startup
    {
        public Startup(IConfiguration configuration) => Configuration = configuration;
        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(option => option.EnableEndpointRouting = false);
            // Registering the Application Database context
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration["Data:Edperm:ConnectionString"])
            );
            //Registering the repository service
            services.AddTransient<ICourseRepository, EFCourseRepository>();
            services.AddTransient<IStudentRepository, EFStudentRepository>();
            services.AddTransient<ISupervisorRepository, EFSupervisorRepository>();
            services.AddTransient<IEnrollmentRepository, EFEnrollmentRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseStaticFiles();           // permet l'utilisation des images, css et js
            app.UseStatusCodePages();
            app.UseMvc(                     // configuration route par défaut
                routes => {
                    //routes for courses
                    // routes.MapRoute(
                    //     name:null,
                    //     template:"{departement}/Page{coursePage:int}",
                    //     defaults: new { Controller = "Course", Action="Index"}
                    // );
                    // routes.MapRoute(
                    //     name:null,
                    //     template:"{Controller}/Page{coursePage:int}",
                    //     defaults: new { Controller = "Course", Action="Index", coursePage=1}
                    // );
                    // // routes.MapRoute(
                    // //     name:null,
                    // //     template:"{departement}",
                    // //     defaults: new { Controller = "Course", Action="Index", coursePage=1}
                    // // );
                    // routes.MapRoute(
                    //     name:null,
                    //     template:"",
                    //     defaults: new { Controller = "Course", Action="Index", coursePage=1}
                    // );
                    // //routes for students
                    // routes.MapRoute(
                    //     name:null,
                    //     template:"{Controller}/Page{studentPage:int}",
                    //     defaults: new { Controller = "Student", Action="Index", studentPage=1}
                    // );
                    // routes.MapRoute(
                    //     name:null,
                    //     template:"",
                    //     defaults: new { Controller = "Student", Action="Index", studentPage=1}
                    // );
                    // //routes for supervisors
                    // routes.MapRoute(
                    //     name:null,
                    //     template:"{Controller}/Page{supPage:int}",
                    //     defaults: new {Controller = "Supervisor", Action="Index", supPage=1}
                    // );
                    // routes.MapRoute(
                    //     name:null,
                    //     template:"",
                    //     defaults: new {Controller = "Supervisor", Action="Index", supPage=1}
                    // );
                    //default route
                    routes.MapRoute(
                        name: null,
                        template : "{controller}/{action}/{id?}"
                    );
                }
            );
            //we populate the database at the begining of the application
            SeedData.InitializeDb(app);
        }
    }
}
