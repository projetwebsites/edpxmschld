/*
    This class will help to transform and display page numbers on view to help with pagination
*/
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using edpxmschdl.Models.ViewModels;
using System.Collections.Generic;

namespace edpxmschdl.Infrastructure.TagHelpers
{
    [HtmlTargetElement("div", Attributes="page-model")]
    public class PaginationTagHelper : TagHelper
    {
        /*
            The IUrlHelper interface provides access to the URL-generating functionality.
        */
        private IUrlHelperFactory m_urlHelperFactory;
        public PaginationTagHelper(IUrlHelperFactory urlHelperFactory) => m_urlHelperFactory = urlHelperFactory;

        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext ViewContext {get; set;}
        public PagingInfo PageModel {get; set;} // present in the div tag in index page as page-model
        public string PageAction {get; set;}    // present in index page as page-action
        //page-url-departement="@Model.CurrentDepartement"
        [HtmlAttributeName(DictionaryAttributePrefix ="page-url-")]
        public Dictionary<string, object> PageUrlValues {get; set;} = new Dictionary<string, object>();
        //public Dictionary<string, string> PageUrlValues {get; set;} = new Dictionary<string, string>();
        public string PageIndexFor {get; set;}
        public bool PageClassesEnabled {get; set;} = false;
        public string PageClass {get; set;}
        public string PageClassNormal {get; set;}
        public string PageClassSelected {get; set;}

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            IUrlHelper urlHelper = m_urlHelperFactory.GetUrlHelper(ViewContext);
            
            TagBuilder result = new TagBuilder("div");
            for(int i = 1; i <= PageModel.TotalPages; i++)
            {
                TagBuilder tag = new TagBuilder("a");

                if(PageIndexFor == "enrollment")
                    PageUrlValues["enrollmentPage"] = i;
                    
                if(PageIndexFor == "course")
                    PageUrlValues["coursePage"] = i; // attribuer la bonne valeur pour multiutilisation

                if(PageIndexFor == "student")
                    PageUrlValues["studentPage"] = i;
                    
                if(PageIndexFor == "supervisor")
                    PageUrlValues["supPage"] = i;

                tag.Attributes["href"] = urlHelper.Action(PageAction, PageUrlValues);
                if(PageClassesEnabled){
                    tag.AddCssClass(PageClass);
                    tag.AddCssClass(i == PageModel.CurrentPage ? PageClassSelected : PageClassNormal);
                }
                tag.InnerHtml.Append(i.ToString()); // chiffre de la page : ex 1 2 3 ...
                result.InnerHtml.AppendHtml(tag);
            }
            output.Content.AppendHtml(result.InnerHtml);
        }
    }
}